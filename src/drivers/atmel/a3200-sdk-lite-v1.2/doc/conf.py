import sys, os

gs_templates_folder= '/usr/local/share/gs_templates'

build_project_name = 'a3200-std'
project_name = u'A3200 SDK Lite'
filename_pdf = u'gs-man-nanomind-a3200-sdk-lite-'

copyright_name = u'2015, GomSpace'
company_name = u'GomSpace'

front_name = "NanoMind"
front_doc_type = "Manual"
front_doc_category = "Software Documentation"
front_image = "sw_front"

execfile(os.path.join(os.path.abspath(gs_templates_folder), 'conf_template.py'))

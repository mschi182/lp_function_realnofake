.. A3200 SDK Lite Documentation master file.

.. |A3200SDK| replace:: A3200 SDK Lite

#####################################
Welcome to |A3200SDK|'s Documentation
#####################################

.. only:: html

   Release |release| - |today|

.. toctree::
   :maxdepth: 3
   
   /doc/a3200-std
   /lib/libasf/doc/libasf
   /lib/liba3200/doc/liba3200
   /lib/libutil/doc/libutil

.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`search`


#include <stdio.h>

#include <conf_a3200.h>

#include <FreeRTOS.h>
#include <task.h>

#include <lm70.h>
#include <mpu3300.h>
#include <hmc5843.h>

static void test_sdram(void) {
	void *heap_start, *heap_end;
	heap_start = (void *) 0xD0000000 + 0x100000;
	heap_end = (void *) 0xD0000000 + 0x2000000 - 1000;

	printf("Writing to SDRAM...\r\n");

	for(int * p = heap_start; p < (int *) heap_end; p++) {
		*p = (uintptr_t) p;
	}

	printf("Reading from SDRAM...\r\n");

	for(int * p = heap_start; p < (int *) heap_end; p++) {
		if ((uintptr_t) p != (uintptr_t) *p) {
			printf("SDRAM ERROR!\r\n");
			return;
		}
	}

	printf("SDRAM OK\r\n");

}

void task_sample_sensors(void * param) {
	int16_t sensor1, sensor2;
	float gyro_temp;
	
	mpu3300_gyro_t gyro_reading;
	hmc5843_data_t hmc_reading;

	while (1) {
		/* Read board temperature sensors */
		sensor1 = lm70_read_temp(1);
		sensor2 = lm70_read_temp(2);

		/* Read gyroscope temperature and rate */
		mpu3300_read_temp(&gyro_temp);
		mpu3300_read_gyro(&gyro_reading);

		/* Read magnetometer */
		hmc5843_read_single(&hmc_reading);

		/* Print readings */
		printf("Temp1: %.1f, Temp2 %.1f, Gyro temp: %.2f\r\n",
				sensor1/10., sensor2/10., gyro_temp);
		printf("Gyro x, y, z: %f, %f, %f\r\n",
				gyro_reading.gyro_x,
				gyro_reading.gyro_y,
				gyro_reading.gyro_z);
		printf("Mag x, y, z: %f, %f, %f\r\n\r\n",
				hmc_reading.x, hmc_reading.y, hmc_reading.z);

		test_sdram();

		/* Wait 1 sec */
		vTaskDelay(1000 / portTICK_RATE_MS);
	}
}

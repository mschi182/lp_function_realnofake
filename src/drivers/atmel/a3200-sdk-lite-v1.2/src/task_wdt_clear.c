#include <FreeRTOS.h>
#include <task.h>

#include "wdt.h"

void task_wdt_clear(void * param) {
	while (1) {
		wdt_clear();
		vTaskDelay(1000 / portTICK_RATE_MS);
	}
}

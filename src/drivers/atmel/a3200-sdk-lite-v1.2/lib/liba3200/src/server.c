/**
 * NanoMind A3200 firmware
 *
 * @author Johan De Claville Christiansen
 * Copyright 2014 GomSpace ApS. All rights reserved.
 */

#include <stdio.h>

#include <util/log.h>
#include <util/clock.h>
#include <csp/csp.h>
#include <csp/csp_endian.h>
#include "wdt.h"

#include <FreeRTOS.h>
#include <task.h>

#include <ftp/ftp_server.h>
#include <param/param.h>
#include <log/rlog.h>
#include <gscript/gscript.h>
#include <a3200.h>
#include <hmc5843.h>
#include <spn_fl512s.h>

#if ENABLE_STORAGE
#include <conf_storage.h>
#include <yaffsfs.h>

void task_mount(void * param) {
	/* Setup spansion chip (assumes SPI has been setup) */
	if (spn_fl512s_init(param_get_uint8(A3200_FS_CHIPID, A3200_BOARD)) != SPN_NO_ERR)
		vTaskDelete(NULL);
	yaffs_start_up();
	if (yaffs_mount("/flash")) {
		log_error("Could not mount /flash");
		param_set_uint8(A3200_FS_MOUNTED, A3200_BOARD, 0);
	} else {
		log_info("Mounted /flash");
		param_set_uint8(A3200_FS_MOUNTED, A3200_BOARD, 1);
	}
	vTaskDelete(NULL);
}

ftp_return_t ftp_yaffs_mkfs(void *state, uint8_t dev) {

	yaffsfs_Lock();
	printf("Erase chip\r\n");
	spn_fl512s_erase_chip();
	printf("Erase chip done\r\n");

	return FTP_RET_OK;
};
#else
void task_mount(void * param) {
	/* Setup spansion chip (assumes SPI has been setup) */
	if (spn_fl512s_init(param_get_uint8(A3200_FS_CHIPID, A3200_BOARD)) != SPN_NO_ERR)
		vTaskDelete(NULL);
	param_set_uint8(A3200_FS_MOUNTED, A3200_BOARD, 0);
	vTaskDelete(NULL);
}
#endif

void sensor_service_handler(csp_conn_t * conn, csp_packet_t * request_packet) {

	/* Read sensor data and return result */
	/* NB: Only supports reading magnetometer */
	hmc5843_data_t magdata;

	csp_packet_t * reply_packet = csp_buffer_get(sizeof(magdata));

	/* Sample magnetometer */
	if (hmc5843_read_single(&magdata) < 0) {
		memcpy(reply_packet->data, &magdata, sizeof(magdata));
	}
	else {
		bzero(reply_packet->data, sizeof(magdata));
	}
	reply_packet->length = sizeof(magdata);
	if (!csp_send(conn, reply_packet, 0))
		csp_buffer_free(reply_packet);
	csp_buffer_free(request_packet);
}

void server_task(void * pvParameters) {

	csp_conn_t * conn;
	csp_packet_t * packet;
	csp_socket_t * socket = csp_socket(0);
	csp_bind(socket, CSP_ANY);
	csp_listen(socket, 10);

	/* Setup FTP server RAM */
	ftp_register_backend(BACKEND_RAM, &backend_ram);
#if ENABLE_STORAGE
	ftp_register_backend(BACKEND_NEWLIB, &backend_newlib);

	/* Register spn erase function as mkfs */ 
	backend_newlib.mkfs = ftp_yaffs_mkfs;
#endif

	while (1) {

		/* Clear wdt before sleeping */
		wdt_clear();

		/* Wait for incoming connection, or timeout */
		conn = csp_accept(socket, 1000);

		/* Only continue if a valid connection is present */
		if (conn == NULL)
			continue;

		/* Spawn new task for FTP */
		if (csp_conn_dport(conn) == A3200_PORT_FTP) {
			extern void task_ftp(void * conn_param);
			if (xTaskCreate(task_ftp, "FTP", 4000, conn, 1, NULL) != pdTRUE)
				csp_close(conn);
			continue;
		}

		/* Read incoming data */
		while ((packet = csp_read(conn, 0)) != NULL) {

			switch (csp_conn_dport(conn)) {

			case A3200_PORT_RPARAM:
				rparam_service_handler(conn, packet);
				break;

			case A3200_PORT_GSCRIPT:
				gscript_service_handler(conn, packet);
				break;

			case A3200_PORT_RLOG:
				rlog_service_handler(conn, packet);
				break;

			case A3200_PORT_SENSOR:
				sensor_service_handler(conn, packet);
				break;

			case A3200_PORT_TIMESYNC: {
				/* Support legacy time sync request */
				/* Note: only return time supported no sync of this node! */

				/* Return current time */
				timestamp_t time;
				clock_get_time(&time);

				/* Send reply */
				time.tv_sec = csp_hton32(time.tv_sec);
				time.tv_nsec = csp_hton32(time.tv_nsec);

				/* Copy to output */
				memcpy(packet->data, &time, sizeof(timestamp_t));
				packet->length = sizeof(timestamp_t);
				if (!csp_send(conn, packet, 0))
					csp_buffer_free(packet);

				break;
			}

			default:
				csp_service_handler(conn, packet);
				break;

			}

		}

		csp_close(conn);

	}

}

/**
 * @file cmd_mpu3300.c
 * Debug command interface mpu3300
 *
 * @author Morten Jensen
 * Copyright 2014 GomSpace ApS. All rights reserved.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>

#include <dev/usart.h>
#include <util/console.h>
#include <FreeRTOS.h>
#include <task.h>

#include <conf_asf.h>
#include "mpu3300.h"

int cmd_mpu3300_read_raw(struct command_context *ctx) {
	mpu3300_gyro_raw_t gyro_raw;

	if (ctx->argc != 1)
		return CMD_ERROR_SYNTAX;

	if (mpu3300_read_raw(&gyro_raw) != 0)
		return CMD_ERROR_FAIL;

	printf("X: %"PRId16"\r\n", gyro_raw.gyro_x);
	printf("Y: %"PRId16"\r\n", gyro_raw.gyro_y);
	printf("Z: %"PRId16"\r\n", gyro_raw.gyro_z);

	return CMD_ERROR_NONE;
}

int cmd_mpu3300_read_loop(struct command_context *ctx) {
	mpu3300_gyro_t gyro;
	mpu3300_gyro_t gyro_nobias;
	mpu3300_gyro_t gyro_angle = {0};
	int i;
	float temp;
	TickType_t xLastWakeTime;
	xLastWakeTime = xTaskGetTickCount();

	float bias[3]={0,0,0};

	if (ctx->argc != 1)
		return CMD_ERROR_SYNTAX;

	for (i=0;i<20;i++)
	{
		mpu3300_read_gyro(&gyro);
		bias[0] += gyro.gyro_x/20.0;
		bias[1] += gyro.gyro_y/20.0;
		bias[2] += gyro.gyro_z/20.0;
		vTaskDelay(configTICK_RATE_HZ * 0.020);
	}

	while (1) {
		vTaskDelayUntil(&xLastWakeTime, configTICK_RATE_HZ * 0.1);

		if (usart_messages_waiting(USART_CONSOLE) != 0) {
			usart_getc(USART_CONSOLE);
			break;
		}


		if (mpu3300_read_gyro(&gyro) != 0)
			return CMD_ERROR_FAIL;
		gyro_nobias.gyro_x = gyro.gyro_x - bias[0];
		gyro_nobias.gyro_y = gyro.gyro_y - bias[1];
		gyro_nobias.gyro_z = gyro.gyro_z - bias[2];

		gyro_angle.gyro_x += gyro_nobias.gyro_x * 0.1;
		gyro_angle.gyro_y += gyro_nobias.gyro_y * 0.1;
		gyro_angle.gyro_z += gyro_nobias.gyro_z * 0.1;


		if (mpu3300_read_temp(&temp) != 0)
			return CMD_ERROR_FAIL;

		printf("Rate:  X: %10.5f, Y: %10.5f, Z: %10.5f\r\n",
				gyro_nobias.gyro_x,
				gyro_nobias.gyro_y,
				gyro_nobias.gyro_z);
		printf("Angle: X: %10.5f, Y: %10.5f, Z: %10.5f\r\n",
				gyro_angle.gyro_x,
				gyro_angle.gyro_y,
				gyro_angle.gyro_z);
		printf("Temperature: %f\r\n", temp);
	}
	return CMD_ERROR_NONE;
}

int cmd_mpu3300_log(struct command_context *ctx) {
	mpu3300_gyro_t gyro;
	mpu3300_gyro_t gyro_nobias;
	mpu3300_gyro_t gyro_angle = {0};
	int i;
	float temp;
	unsigned long int k = 0;
	TickType_t xLastWakeTime;
	xLastWakeTime = xTaskGetTickCount();

	float bias[3]={0,0,0};

	if (ctx->argc != 1)
		return CMD_ERROR_SYNTAX;

	for (i=0;i<20;i++)
	{
		mpu3300_read_gyro(&gyro);
		bias[0] += gyro.gyro_x/20.0;
		bias[1] += gyro.gyro_y/20.0;
		bias[2] += gyro.gyro_z/20.0;
		vTaskDelay(configTICK_RATE_HZ * 0.020);
	}

	while (1) {
		vTaskDelayUntil(&xLastWakeTime, configTICK_RATE_HZ * 1);
		k++;

		if (usart_messages_waiting(USART_CONSOLE) != 0) {
			usart_getc(USART_CONSOLE);
			break;
		}


		if (mpu3300_read_gyro(&gyro) != 0)
			return CMD_ERROR_FAIL;
		gyro_nobias.gyro_x = gyro.gyro_x - bias[0];
		gyro_nobias.gyro_y = gyro.gyro_y - bias[1];
		gyro_nobias.gyro_z = gyro.gyro_z - bias[2];

		gyro_angle.gyro_x += gyro_nobias.gyro_x * 1;
		gyro_angle.gyro_y += gyro_nobias.gyro_y * 1;
		gyro_angle.gyro_z += gyro_nobias.gyro_z * 1;


		if (mpu3300_read_temp(&temp) != 0)
			return CMD_ERROR_FAIL;

		printf("%lu, %10.5f, %10.5f, %10.5f, %10.5f, %10.5f, %10.5f, %f\r\n",
				k,
				gyro_nobias.gyro_x,
				gyro_nobias.gyro_y,
				gyro_nobias.gyro_z,
				gyro_angle.gyro_x,
				gyro_angle.gyro_y,
				gyro_angle.gyro_z,
				temp);
	}
	return CMD_ERROR_NONE;
}



int cmd_mpu3300_calib(struct command_context *ctx) {
	mpu3300_gyro_t gyro;

	int i;

	float bias[3]={0,0,0};

	if (ctx->argc != 1)
		return CMD_ERROR_SYNTAX;

	for (i=0;i<50;i++)
	{
		mpu3300_read_gyro(&gyro);
		bias[0] -= gyro.gyro_x/50.0;
		bias[1] -= gyro.gyro_y/50.0;
		bias[2] -= gyro.gyro_z/50.0;
		vTaskDelay(configTICK_RATE_HZ * 0.020);
	}

	printf("Bias x %f\r\n", bias[0]);
	printf("Bias y %f\r\n", bias[1]);
	printf("Bias z %f\r\n", bias[2]);

	return CMD_ERROR_NONE;
}

int cmd_mpu3300_read(struct command_context *ctx) {
	mpu3300_gyro_t gyro_reading;

	if (ctx->argc != 1)
		return CMD_ERROR_SYNTAX;

	if (mpu3300_read_gyro(&gyro_reading) != 0)
		return CMD_ERROR_FAIL;

	printf("X: %10.5f, Y: %10.5f, Z: %10.5f\r\n", 
			gyro_reading.gyro_x, 
			gyro_reading.gyro_y, 
			gyro_reading.gyro_z);

	return CMD_ERROR_NONE;
}

int cmd_mpu3300_read_temp(struct command_context *ctx) {
	float temp;

	if (ctx->argc != 1)
		return CMD_ERROR_SYNTAX;

	if (mpu3300_read_temp(&temp) != 0)
		return CMD_ERROR_FAIL;

	printf("Temperature: %f\r\n", temp);

	return CMD_ERROR_NONE;
}

int cmd_mpu3300_reset(struct command_context *ctx) {

	if (ctx->argc != 1)
		return CMD_ERROR_SYNTAX;

	if (mpu3300_reset() != 0)
		return CMD_ERROR_FAIL;

	return CMD_ERROR_NONE;
}

int cmd_mpu3300_sleep(struct command_context *ctx) {
	int sleep;

	if (ctx->argc != 2)
		return CMD_ERROR_SYNTAX;

	sleep = atoi(ctx->argv[1]);
	if (mpu3300_sleep(sleep) != 0)
		return CMD_ERROR_FAIL;

	return CMD_ERROR_NONE;
}

int cmd_mpu3300_init(struct command_context *ctx) {
	int bw;
	int scale;

	if (ctx->argc != 3)
		return CMD_ERROR_SYNTAX;

	bw = atoi(ctx->argv[1]);
	scale = atoi(ctx->argv[2]);

	if (bw < 0 || bw > 6)
		return CMD_ERROR_SYNTAX;

	if (scale < 0 || scale > 1)
		return CMD_ERROR_SYNTAX;

	if (mpu3300_init(bw, scale) != 0)
		return CMD_ERROR_FAIL;

	return CMD_ERROR_NONE;
}

int cmd_mpu3300_selftest(struct command_context *ctx) {
	int res;

	if (ctx->argc != 1)
		return CMD_ERROR_SYNTAX;

	res = mpu3300_selftest();
	if (res == -1)
		return CMD_ERROR_FAIL;

	if (res == -2) {
		printf("Self test failed\r\n");
	}
	printf("Self test OK\r\n");

	return CMD_ERROR_NONE;
}


command_t __sub_command mpu3300_subcommands[] = {
		{
				.name = "readraw",
				.help = "Raw gyro readings",
				.handler = cmd_mpu3300_read_raw,
		},{
				.name = "read",
				.help = "Gyro readings",
				.handler = cmd_mpu3300_read,
		},{
				.name = "cal",
				.help = "Calibrate gyro",
				.handler = cmd_mpu3300_calib,
		},{
				.name = "readloop",
				.help = "Gyro reading (loop)",
				.handler = cmd_mpu3300_read_loop,
		},{
				.name = "log",
				.help = "Gyro logging",
				.handler = cmd_mpu3300_log,
		},{
				.name = "temp",
				.help = "Read gyro temperature",
				.handler = cmd_mpu3300_read_temp,
		},{
				.name = "reset",
				.help = "Reset gyro",
				.handler = cmd_mpu3300_reset,
		},{
				.name = "sleep",
				.help = "Gyro sleep",
				.usage = "<sleep 1=sleep, 0=wake>",
				.handler = cmd_mpu3300_sleep,
		},{
				.name = "init",
				.help = "Init gyro",
				.usage = "<bw> <full_scale> (bw: 0=256, 1=188, 2=98, 3=42, 4=20, 5=10, 5=6, scale: 0=255, 1=450) ",
				.handler = cmd_mpu3300_init,
		},{
				.name = "selftest",
				.help = "Gyro self test",
				.handler = cmd_mpu3300_selftest,
		},



};

command_t __sub_command __root_command mpu3300_commands[] = {
		{
				.name = "gyro",
				.help = "MPU3300 gyro commands",
				.chain = INIT_CHAIN(mpu3300_subcommands),
		},
};

void cmd_mpu3300_setup(void) {
	command_register(mpu3300_commands);
}

/*
 * cmd_pwm.c
 *
 *  Created on: Jun 19, 2014
 *      Author: bisgaard
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>

#include <util/console.h>

#include "adc_channels.h"

int adc_read(struct command_context *ctx) {
	int16_t adcval[ADC_NCHANS];
	adc_channels_sample(adcval);
	for(int i = 0; i < ADC_NCHANS; i++)
		printf("ADC %i = %d\r\n", i, adcval[i]);
	return CMD_ERROR_NONE;
}

command_t __sub_command adc_subcommands[] = {
	{
		.name = "read",
		.help = "Read",
		.handler = adc_read
	}
};

command_t __sub_command __root_command adc_commands[] = {
	{
		.name = "adc",
		.help = "ADC commands",
		.chain = INIT_CHAIN(adc_subcommands),
	},
};

void cmd_adc_setup(void) {
	command_register(adc_commands);
}

/**
 * @file cmd_fm33256b.c
 * Debug command interface for the Maxim FM33256B RTC
 *
 * @author Jeppe Ledet-Pedersen
 * @author Morten Jensen
 * Copyright 2013 GomSpace ApS. All rights reserved.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <limits.h>
#include <inttypes.h>
#include <string.h>
#include <time.h>

#include <util/console.h>
#include <util/hexdump.h>
#include "fm33256b.h"

int cmd_rtc_set(struct command_context *ctx) {

	unsigned long time;
	char *remain;
	struct fm33256b_clock clock;

	if (ctx->argc != 2)
		 return CMD_ERROR_SYNTAX;

	time = strtoul(ctx->argv[1], &remain, 10);
	if (time == ULONG_MAX || remain == ctx->argv[1])
		return CMD_ERROR_SYNTAX;

	if (fm33256b_time_to_clock((time_t *) &time, &clock) < 0)
		return CMD_ERROR_FAIL;

	if (fm33256b_clock_write_burst(&clock) < 0)
		return CMD_ERROR_FAIL;

	return CMD_ERROR_NONE;
}

int cmd_rtc_get(struct command_context *ctx) {
	
	unsigned long time;
	struct fm33256b_clock clock;
	
	if (fm33256b_clock_read_burst(&clock) < 0)
		return CMD_ERROR_FAIL;

	if (fm33256b_clock_to_time((time_t *) &time, &clock) < 0)
		return CMD_ERROR_FAIL;

	printf("Time is: %s\r\n", ctime((time_t *) &time));

	return CMD_ERROR_NONE;
}

int cmd_rtc_debug_status(struct command_context *ctx) {
	uint8_t cmd = FM33_RDSR;
	uint8_t res = 0;
	fm33256b_read(&cmd, 1, &res, 1);
	printf("Status reg: %d\r\n", res);	
	return CMD_ERROR_NONE;
}

int cmd_rtc_debug_clk_halt(struct command_context *ctx) {
	fm33256b_clock_halt();
	return CMD_ERROR_NONE;
}

int cmd_rtc_debug_clk_resume(struct command_context *ctx) {
	fm33256b_clock_resume();
	return CMD_ERROR_NONE;
}

int cmd_rtc_debug_read_reg(struct command_context *ctx) {
	uint8_t cmd[2];
	uint8_t res;
	uint32_t addr;
	if (ctx->argc != 2)
		 return CMD_ERROR_SYNTAX;

	sscanf(ctx->argv[1], "%lx", &addr);

	cmd[0] = FM33_RDPC;
	cmd[1] = addr;
	fm33256b_read(cmd, 2, &res, 1);
	printf("Reg %lx: %x\r\n", addr, res);
	return CMD_ERROR_NONE;
}

int cmd_rtc_debug_write_reg(struct command_context *ctx) {
	uint8_t cmd[3];
	uint32_t addr;
	uint32_t val;

	if (ctx->argc != 3)
		 return CMD_ERROR_SYNTAX;

	sscanf(ctx->argv[1], "%lx", &addr);
	sscanf(ctx->argv[2], "%lx", &val);
		
	cmd[0] = FM33_WRPC;
	cmd[1] = addr;
	cmd[2] = val & 0xff;

	fm33256b_write(cmd, 3);

	return CMD_ERROR_NONE;
}


int cmd_rtc_write(struct command_context *ctx) {

	uint32_t addr;
	char data[256] = {};
	
	if (ctx->argc != 3)
		return CMD_ERROR_SYNTAX;

	sscanf(ctx->argv[1], "%lx", &addr);
	sscanf(ctx->argv[2], "%[^\t\n]", data);

	int len = strlen(ctx->argv[2]) / 2;
	unsigned int to_int(char c) {
		if (c >= '0' && c <= '9') return      c - '0';
		if (c >= 'A' && c <= 'F') return 10 + c - 'A';
		if (c >= 'a' && c <= 'f') return 10 + c - 'a';
		return -1;
	}

	for (int i = 0; ((i < len) && (i < 256)); ++i)
		data[i] = 16 * to_int(ctx->argv[2][2*i]) + to_int(ctx->argv[2][2*i+1]);

	fm33256b_write_data((uint16_t)addr, (uint8_t *) data, len);

	return CMD_ERROR_NONE;
}

int cmd_rtc_read(struct command_context *ctx) {
	uint8_t data[256];
	uint32_t addr;
	
	if (ctx->argc != 2)
		return CMD_ERROR_SYNTAX;

	sscanf(ctx->argv[1], "%lx", &addr);
	printf("%lx\r\n", addr);
	fm33256b_read_data((uint16_t)addr, data, 256);
	
	hex_dump(data, 256);

	return CMD_ERROR_NONE;
}

command_t __sub_command rtc_clock_subcommands[] = {
	{
		.name = "set",
		.help = "Set RTC",
		.handler = cmd_rtc_set,
		.usage = "<time>",
	},{
		.name = "get",
		.help = "Get current RTC value",
		.handler = cmd_rtc_get,
	}
};

command_t __sub_command rtc_debug_subcommands[] = {
	{
		.name = "SR",
		.help = "Read Status Reg.",
		.handler = cmd_rtc_debug_status,
	},{
		.name = "clkresume",
		.help = "Oscillator Enable.",
		.handler = cmd_rtc_debug_clk_resume,
	},{
		.name = "clkhalt",
		.help = "Oscillator Disable.",
		.handler = cmd_rtc_debug_clk_halt,
	},{
		.name = "readreg",
		.help = "Read a register",
		.handler = cmd_rtc_debug_read_reg,
		.usage = "<reg_addr_hex>",
	},{
		.name = "writereg",
		.help = "Write a register",
		.handler = cmd_rtc_debug_write_reg,
		.usage = "<reg_addr_hex> <data [hex]>",
	}

};

command_t __sub_command rtc_data_subcommands[] = {
	{
		.name = "read",
		.help = "Read RTC Data",
		.usage = "<addr [hex]>",
		.handler = cmd_rtc_read,
	},{
		.name = "write",
		.help = "Write RTC Data",
		.usage = "<addr [hex]> <data> (max len 256)",
		.handler = cmd_rtc_write,
	}
};

command_t __sub_command rtc_subcommands[] = {
	{
		.name = "clock",
		.help = "Clock subcommands",
		.chain = INIT_CHAIN(rtc_clock_subcommands),
	},{
		.name = "debug",
		.help = "Debug subcommands",
		.chain = INIT_CHAIN(rtc_debug_subcommands),
	},{
		.name = "data",
		.help = "Data subcommands",
		.chain = INIT_CHAIN(rtc_data_subcommands),
	}
};

command_t __sub_command __root_command rtc_commands[] = {
	{
		.name = "rtc",
		.help = "FM33256B RTC commands",
		.chain = INIT_CHAIN(rtc_subcommands),
	},
};

void cmd_rtc_setup(void) {
	command_register(rtc_commands);
}

/**
 * ADC channel driver
 *
 * @author Johan de Claville Christiansen
 * Copyright 2014 GomSpace ApS. All rights reserved.
 */

#include <reset_cause.h>
#include <spi.h>
#include <dev/i2c.h>
#include <gpio.h>
#include <board.h>

#include <pwr_switch.h>
#include <log/log.h>
#include <lm70.h>
#include <mpu3300.h>
#include <hmc5843.h>
#include <fm33256b.h>
#include <adc_channels.h>
#include <mb_switch.h>
#include <util/hexdump.h>
#include <scif_uc3c.h>

#include <conf_liba3200.h>
#if PARAM_ENABLED
	#include <param/param.h>
	#include <a3200_board.h>
#endif
#include <swload.h>

#include <util/log.h>
LOG_GROUP_MASKED(reset, "reset", LOG_ALL_MASK, LOG_ALL_MASK);

#ifndef SWLOAD_LINK_ADDRESS
#define SWLOAD_LINK_ADDRESS	0xD0000000
#define SWLOAD_LINK_SIZE    0x00100000
#endif

#define MAX_SWLOAD_COUNT 100

static void init_reset_casue(int reset_causes) {

	if (reset_causes & CHIP_RESET_CAUSE_EXTRST)
		log_error_group(reset, "External reset");

	if (reset_causes & AVR32_PM_RCAUSE_BOD_MASK)
		log_error_group(reset, "Brown-out detected on CPU power domain");

	if (reset_causes & AVR32_PM_RCAUSE_BOD33_MASK)
		log_error_group(reset, "Brown-out detected on I/O power domain");

	if (reset_causes & AVR32_PM_RCAUSE_OCDRST_MASK)
		log_info_group(reset, "On-chip debug system");

	if (reset_causes & AVR32_PM_RCAUSE_POR_MASK)
		log_info_group(reset, "Power-on-reset");

	if (reset_causes & CHIP_RESET_CAUSE_JTAG)
		log_info_group(reset, "JTAG");

	if (reset_causes & AVR32_PM_RCAUSE_WDT_MASK)
		log_error_group(reset, "Watchdog timeout");

	if (reset_causes & AVR32_PM_RCAUSE_CPUERR_MASK)
		log_error_group(reset, "CPU Error");

	if (reset_causes & AVR32_PM_RCAUSE_SLEEP_MASK)
		log_info_group(reset, "Wake from Shutdown sleep mode");

}

#if BOARDREV >= 3
static void init_spi0(void) {
	/* Setting up the GPIO map for SPI on the board */
	gpio_map_t spi_piomap = {
		{AVR32_SPI0_MOSI_2_PIN, AVR32_SPI0_MOSI_2_FUNCTION},
		{AVR32_SPI0_MISO_2_PIN, AVR32_SPI0_MISO_2_FUNCTION},
		{AVR32_SPI0_SCK_2_PIN, AVR32_SPI0_SCK_2_FUNCTION},
		{AVR32_SPI0_NPCS_0_2_PIN, AVR32_SPI0_NPCS_0_2_FUNCTION},
		{AVR32_SPI0_NPCS_1_2_PIN, AVR32_SPI0_NPCS_1_2_FUNCTION},
		{AVR32_SPI0_NPCS_2_2_PIN, AVR32_SPI0_NPCS_2_2_FUNCTION},
	};
	gpio_enable_module(spi_piomap, 6);

	/* Setup of SPI controller one */
	sysclk_enable_pba_module(SYSCLK_SPI0);
	spi_reset(&AVR32_SPI0);
	spi_set_master_mode(&AVR32_SPI0);
	spi_disable_modfault(&AVR32_SPI0);
	spi_disable_loopback(&AVR32_SPI0);
	spi_set_chipselect(&AVR32_SPI0, (1 << AVR32_SPI_MR_PCS_SIZE) - 1);
	spi_disable_variable_chipselect(&AVR32_SPI0);
	spi_disable_chipselect_decoding(&AVR32_SPI0);
	spi_enable(&AVR32_SPI0);
}
#endif

static void init_spi1(void) {
	/* Setting up the GPIO map for SPI on the board */
	gpio_map_t spi_piomap = {
		{AVR32_SPI1_SCK_0_1_PIN, AVR32_SPI1_SCK_0_1_FUNCTION},
		{AVR32_SPI1_MOSI_0_1_PIN, AVR32_SPI1_MOSI_0_1_FUNCTION},
		{AVR32_SPI1_MOSI_0_2_PIN, AVR32_SPI1_MOSI_0_2_FUNCTION},
		{AVR32_SPI1_MISO_0_1_PIN, AVR32_SPI1_MISO_0_1_FUNCTION},
		{AVR32_SPI1_NPCS_2_2_PIN, AVR32_SPI1_NPCS_2_2_FUNCTION},
		{AVR32_SPI1_NPCS_3_2_PIN, AVR32_SPI1_NPCS_3_2_FUNCTION},
		{AVR32_SPI1_NPCS_0_PIN, AVR32_SPI1_NPCS_0_FUNCTION},
		{AVR32_SPI1_NPCS_1_2_PIN, AVR32_SPI1_NPCS_1_2_FUNCTION},
	};
	gpio_enable_module(spi_piomap, 8);

	/* Setup of SPI controller one */
	sysclk_enable_pba_module(SYSCLK_SPI1);
	spi_reset(&AVR32_SPI1);
	spi_set_master_mode(&AVR32_SPI1);
	spi_disable_modfault(&AVR32_SPI1);
	spi_disable_loopback(&AVR32_SPI1);
	spi_set_chipselect(&AVR32_SPI1, (1 << AVR32_SPI_MR_PCS_SIZE) - 1);
	spi_disable_variable_chipselect(&AVR32_SPI1);
	spi_disable_chipselect_decoding(&AVR32_SPI1);
	spi_enable(&AVR32_SPI1);
}

static void init_rtc(void) {

	/* Setup RTC */
	uint8_t cmd[] = {FM33_WRPC, 0x18, 0x3D};
	fm33256b_write(cmd, 3);

	/* RTC */
	fm33256b_clock_resume();

	/* 32kHz Crystal setup */
	osc_enable(OSC_ID_OSC32);
}

static void init_twi(void) {
	/* GPIO map setup */
	const gpio_map_t TWIM_GPIO_MAP = {
		{AVR32_TWIMS2_TWCK_0_0_PIN, AVR32_TWIMS2_TWCK_0_0_FUNCTION},
		{AVR32_TWIMS2_TWD_0_0_PIN, AVR32_TWIMS2_TWD_0_0_FUNCTION}
	};
	gpio_enable_module(TWIM_GPIO_MAP, sizeof(TWIM_GPIO_MAP) / sizeof(TWIM_GPIO_MAP[0]));

	/* Init twi master controller 2 with addr 5 and 100 kHz clock */
	i2c_init_master(2, 5, 150);
}


#if PARAM_ENABLED
static void init_board_param(void) {

	static char board[A3200_MEM_SIZE] __attribute__((aligned(8))) = {};

	/* Init parameter system */
	param_index_set(A3200_BOARD, (param_index_t) {.table = a3200_config, .count = A3200_CONFIG_COUNT, .physaddr = board, .size = A3200_MEM_SIZE, .framaddr = FRAM_FNO_BOARD_PERSIST_ADDR});
	param_init_persist(param_ptr(A3200_BOARD));

	/* Register change callbacks */
	void config_callback(uint16_t addr, param_index_t * mem);
	param_ptr(A3200_BOARD)->callback = &a3200_board_callback;

}
#endif

static void init_swload(void * param) {
	int count = param_get_uint16(A3200_SWLOAD_COUNT, A3200_BOARD);
	int mount_wait_s = 300;

	if (count > MAX_SWLOAD_COUNT) {
		log_error("swboot_count was set too high: %u, setting it to %d", count, MAX_SWLOAD_COUNT);
		count = 100;
		param_set_uint16_persist(A3200_SWLOAD_COUNT, A3200_BOARD, count);
	}

	if (count > 0) {
		char * path = param_get_string(A3200_SWLOAD_IMAGE, A3200_BOARD);

		log_info("Trying to boot from %s, %u times left", path, count);
		param_set_uint16_persist(A3200_SWLOAD_COUNT, A3200_BOARD, count - 1);

		log_info("Waiting for fs mount");
		while (mount_wait_s-- && !param_get_uint8(A3200_FS_MOUNTED, A3200_BOARD)) {
			vTaskDelay(1 * configTICK_RATE_HZ);
		}

		/* If we have a mounted fs try swload */
		if (param_get_uint8(A3200_FS_MOUNTED, A3200_BOARD)) {
			log_info("FS mounted resuming swload");
			swload_file(path, (void *)SWLOAD_LINK_ADDRESS, SWLOAD_LINK_SIZE);
		} else {
			log_error("FS not mounted within timeout");
		}
	}

	vTaskDelete(NULL);
}

void init_can(int enable_can) {

	/* Setup the generic clock for CAN */
	scif_gc_setup(AVR32_SCIF_GCLK_CANIF,
			SCIF_GCCTRL_OSC0,
			AVR32_SCIF_GC_NO_DIV_CLOCK,
			0);
	if (enable_can) {
		/* Enable the generic clock */
		scif_gc_enable(AVR32_SCIF_GCLK_CANIF);

		/* Setup GPIO map for CAN connection in stack */
		static const gpio_map_t CAN_GPIO_MAP =
		{
				{AVR32_CANIF_RXLINE_0_0_PIN, AVR32_CANIF_RXLINE_0_0_FUNCTION},
				{AVR32_CANIF_TXLINE_0_0_PIN, AVR32_CANIF_TXLINE_0_0_FUNCTION}
		};
		/* Assign GPIO to CAN */
		gpio_enable_module(CAN_GPIO_MAP, sizeof(CAN_GPIO_MAP) / sizeof(CAN_GPIO_MAP[0]));

		/* Initialize PA15 (CANMODE) GPIO, must be pulled low to enable CAN Transceiver TI SN65HVD230 */
		gpio_configure_pin(AVR32_PIN_PA15, GPIO_DIR_OUTPUT | GPIO_INIT_LOW);
	} else {
		/* Stop the generic clock */
		scif_stop_gclk(AVR32_SCIF_GCLK_CANIF);
		/* Ensure can output is low to disable/power down transceiver */
		gpio_configure_pin(AVR32_PIN_PA15, (GPIO_DIR_OUTPUT | GPIO_INIT_HIGH));
	}
}

void board_init(void) {

	/* SPI device drivers */
#if BOARDREV >= 3
	init_spi0();
#endif
	init_spi1();

	/* Init temperature sensors */
	lm70_init();

	/* Init RTC and FRAM chip */
	fm33256b_init();
	init_rtc();

	/* Latest reset source */
	init_reset_casue(reset_cause_get_causes());

#if PARAM_ENABLED
	/* Board parameters */
	init_board_param();
#endif

	/* Log init */
#if PARAM_ENABLED
	if (param_get_uint8(A3200_LOG_FRAM, A3200_BOARD) > 0) {
		log_store_init(0x10000000 + A3200_FRAM_LOG, A3200_FRAM_LOG_SIZE);
	} else {
		log_store_init((vmemptr_t) calloc(1, A3200_FRAM_LOG_SIZE), A3200_FRAM_LOG_SIZE);
	}
#endif

	/* Init I2C controller for gyroscope, magnetometer and GSSB devices */
	init_twi();

	/* Init can, default disabled */
	init_can(0);

#if MPU3300_ENABLED
	/* Init gyroscope */
	mpu3300_init(MPU3300_BW_5, MPU3300_FSR_225);
#endif

#if HMC5843_ENABLED
	/* Init magnetometer */
	hmc5843_init();
#endif

	/* Setup ADC channels for current measurements */
	adc_channels_init();

	/* Setup motherboard switches */
#if MBSWITCH_ENABLED
	mb_switch_init();
#endif

	/* Log bootcause */
#if PARAM_ENABLED
	param_set_uint32(A3200_BOOT_CAUSE, A3200_BOARD, reset_cause_get_causes());
	param_set_uint16_persist(A3200_BOOT_COUNTER, A3200_BOARD, param_get_uint16(A3200_BOOT_COUNTER, A3200_BOARD) + 1);
#endif

	/* Try software load */
#ifndef RAM
	xTaskCreate(init_swload, "SWLOAD", 2000, NULL, 1, NULL);
#endif

#ifdef RAM
	log_info("Ram image");
#endif

}

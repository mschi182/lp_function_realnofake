/**
 * Power switch driver
 *
 * @author Jesper Wramberg & Mathias Tausen
 * Copyright 2014 GomSpace ApS. All rights reserved.
 */

#include <gpio.h>
#include "pwr_switch.h"
#include "user_board.h"
#include <conf_liba3200.h>
#if PARAM_ENABLED
	#include <param/param.h>
	#include <a3200_board.h>
#endif

void pwr_switch_init(void) {
	gpio_configure_pin(PWR_GSSB_PIN, GPIO_DIR_OUTPUT | GPIO_INIT_LOW);
    gpio_configure_pin(PWR_SD_PIN, GPIO_DIR_OUTPUT | GPIO_INIT_LOW);
    gpio_configure_pin(PWR_GSSB2_PIN, GPIO_DIR_OUTPUT | GPIO_INIT_LOW);
#if BOARDREV >= 3
    gpio_configure_pin(PWR_PWM_PIN, GPIO_DIR_OUTPUT | GPIO_INIT_LOW);
#endif
}

void pwr_switch_enable(pwr_switch_names_t pwr_switch) {
    switch (pwr_switch) {
        case PWR_GSSB:
            gpio_set_pin_high(PWR_GSSB_PIN);
#if PARAM_ENABLED
            param_set_uint8_nocallback(A3200_PWR_GSSB1, A3200_BOARD, 1);
#endif
            break;
        case PWR_SD:
            gpio_set_pin_high(PWR_SD_PIN);
#if PARAM_ENABLED
            param_set_uint8_nocallback(A3200_PWR_FLASH, A3200_BOARD, 1);
#endif
            break;
        case PWR_GSSB2:
            gpio_set_pin_high(PWR_GSSB2_PIN);
#if PARAM_ENABLED
            param_set_uint8_nocallback(A3200_PWR_GSSB2, A3200_BOARD, 1);
#endif
            break;
#if BOARDREV >= 3
        case PWR_PWM:
        	gpio_set_pin_high(PWR_PWM_PIN);
#if PARAM_ENABLED
        	param_set_uint8_nocallback(A3200_PWR_PWM, A3200_BOARD, 1);
#endif
        	break;
#endif
    }
}

void pwr_switch_disable(pwr_switch_names_t pwr_switch) {
    switch (pwr_switch) {
        case PWR_GSSB:
            gpio_set_pin_low(PWR_GSSB_PIN);
#if PARAM_ENABLED
            param_set_uint8_nocallback(A3200_PWR_GSSB1, A3200_BOARD, 0);
#endif
            break;
        case PWR_SD:
            gpio_set_pin_low(PWR_SD_PIN);
#if PARAM_ENABLED
            param_set_uint8_nocallback(A3200_PWR_FLASH, A3200_BOARD, 0);
#endif
            break;
        case PWR_GSSB2:
            gpio_set_pin_low(PWR_GSSB2_PIN);
#if PARAM_ENABLED
            param_set_uint8_nocallback(A3200_PWR_GSSB2, A3200_BOARD, 0);
#endif
            break;
#if BOARDREV >= 3
        case PWR_PWM:
		gpio_set_pin_low(PWR_PWM_PIN);
#if PARAM_ENABLED
		param_set_uint8_nocallback(A3200_PWR_PWM, A3200_BOARD, 0);
#endif
		break;
#endif
    }
}



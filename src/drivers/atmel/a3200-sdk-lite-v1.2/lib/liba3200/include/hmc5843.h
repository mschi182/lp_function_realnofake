/* hmc5843.h
 *
 *  Created on: Aug 31, 2009
 *      Author: karl
 */

#ifndef HMC5843_H_
#define HMC5843_H_

//#include <command/command.h>
/**
 * @file
 * @brief Driver interface for hmc5843
 * \addtogroup hmc5843
 *
 * @{
 */

/**
 * Rates
 */
typedef enum hmc5843_rate_e {
    MAG_RATE_0_5 = 0,//!< MAG_RATE_0_5
    MAG_RATE_1   = 1,//!< MAG_RATE_1
    MAG_RATE_2   = 2,//!< MAG_RATE_2
    MAG_RATE_5   = 3,//!< MAG_RATE_5
    MAG_RATE_10  = 4,//!< MAG_RATE_10
    MAG_RATE_20  = 5,//!< MAG_RATE_20
    MAG_RATE_50  = 6,//!< MAG_RATE_50
} hmc5843_rate_t;

/**
 * Measurement modes
 */
typedef enum hmc5843_meas_e {
    MAG_MEAS_NORM = 0,//!< MAG_MEAS_NORM
    MAG_MEAS_POS  = 1,//!< MAG_MEAS_POS
    MAG_MEAS_NEG  = 2 //!< MAG_MEAS_NEG
} hmc5843_meas_t;


/**
 * Gain settings
 */
typedef enum hmc5843_gain_e {
	MAG_GAIN_0_7  = 0,//!< MAG_GAIN_0_7 (max value is 0.7 G)
	MAG_GAIN_1_0  = 1,//!< MAG_GAIN_1_0 (max value is 1.0 G)
	MAG_GAIN_1_5  = 2,//!< MAG_GAIN_1_5 (max value is 1.5 G)
	MAG_GAIN_2_0  = 3,//!< MAG_GAIN_2_0 (max value is 2.0 G)
	MAG_GAIN_3_2  = 4,//!< MAG_GAIN_3_2 (max value is 3.2 G)
	MAG_GAIN_3_8  = 5,//!< MAG_GAIN_3_8 (max value is 3.8 G)
	MAG_GAIN_4_5  = 6,//!< MAG_GAIN_4_5 (max value is 4.5 G)
	MAG_GAIN_6_5  = 7 //!< MAG_GAIN_6_5 (max value is 6.5 G)
} hmc5843_gain_t;

/**
 * Mode Settings
 */
typedef enum mag_mode_e {
	MAG_MODE_CONTINUOUS = 0,
	MAG_MODE_SINGLE     = 1,
	MAG_MODE_IDLE       = 2,
	MAG_MODE_SLEEP      = 3
} hmc5843_mode_t;

/**
 * Set rate of magnetometer, use the mag_rate_t enum type
 * @param rate
 * @param meas
 * @param gain
 * @return Error code E_NO_ERR if set is successful.
 */
int hmc5843_set_conf(hmc5843_rate_t new_rate, hmc5843_meas_t new_meas, hmc5843_gain_t new_gain);
hmc5843_gain_t hmc5843_get_gain();
hmc5843_rate_t hmc5843_get_rate();
hmc5843_meas_t hmc5843_get_meas();

/**
 * Set mode of magnetometer
 * @param new_mode
 */
int hmc5843_set_mode(hmc5843_mode_t new_mode);

/**
 * Struct for returning magnetometer readings.
 * Values are in milli Gauss
 */
typedef struct hmc5843_data_s {
	float x;
	float y;
	float z;
} hmc5843_data_t;

/**
 * Initialize device only if it has not been initialized before.
 * @return Error code E_NO_ERR if init is successful.
 */
int hmc5843_init(void);

/**
 * Force initialize of device
 * @return Error code E_NO_ERR if init is successful.
 */
int hmc5843_init_force(void);

/**
 * Request a single reading.
 *
 * @param data Pointer to a mag_data_t structure where reading will be returned
 * @return Error code E_NO_ERR if reading is successful.
 */
int hmc5843_read_single(hmc5843_data_t * data);
int hmc5843_read(hmc5843_data_t * data);
int hmc5843_read_raw(hmc5843_data_t * data);
int hmc5843_read_test(hmc5843_data_t * data);

/**
 * Test programs
 */
//int hmc5843_loop(struct command_context *ctx);
//int hmc5843_loop_fast(struct command_context *ctx);
//int hmc5843_loop_raw(struct command_context *ctx);
//int hmc5843_loop_noformat(struct command_context *ctx);
//int hmc5843_test_single(struct command_context *ctx);
//int hmc5843_test_single_alt(struct command_context *ctx);
//int hmc5843_test_bias(struct command_context *ctx);
//int hmc5843_get_info(struct command_context *ctx);


/**
 * }@
 */



#endif /* HMC5843_H_ */

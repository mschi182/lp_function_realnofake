/**
 * Power switch driver
 *
 * @author Jesper Wramberg & Mathias Tausen
 * Copyright 2014 GomSpace ApS. All rights reserved.
 */

#ifndef PWR_SWITCH_H_
#define PWR_SWITCH_H_

#include <conf_liba3200.h>

typedef enum {
	PWR_GSSB = 0,
	PWR_SD = 1,
	PWR_GSSB2 = 2,
#if BOARDREV >= 3
	PWR_PWM = 3,
#endif
} pwr_switch_names_t ;

/**
 * Run this before the other functions
 */
void pwr_switch_init(void);

/**
 * Turn PWR switch on
 * @param pwr_switch use ENUM for pwr_switch names
 */
void pwr_switch_enable(pwr_switch_names_t pwr_switch);

/**
 * Turn PWR switch off
 * @param pwr_switch use ENUM for pwr_switch names
 */
void pwr_switch_disable(pwr_switch_names_t pwr_switch);

#endif /* PWR_SWITCH_H_ */

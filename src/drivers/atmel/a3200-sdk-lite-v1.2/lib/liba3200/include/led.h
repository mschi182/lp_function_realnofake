/**
 * NanoCom firmware
 *
 * @author Johan De Claville Christiansen
 * Copyright 2014 GomSpace ApS. All rights reserved.
 */

#ifndef LED_H_
#define LED_H_

/** GPIO pin mapping for LED's */
#define LED_PIN_0	74
#define LED_PIN_1	76

typedef enum {
	LED_CPUOK,
	LED_A,
} led_names_t ;

/**
 * Run this before the other functions
 */
void led_init(void);

/**
 * Turn LED on
 * @param led use ENUM for led names
 */
void led_on(led_names_t led);

/**
 * Turn LED off
 * @param led use ENUM for led names
 */
void led_off(led_names_t led);

/**
 * Toggle LED
 * @param led use ENUM for led names
 */
void led_toggle(led_names_t led);

#endif /* LED_H_ */

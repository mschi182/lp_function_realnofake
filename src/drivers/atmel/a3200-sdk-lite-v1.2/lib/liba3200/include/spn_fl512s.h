#ifndef SPN_FL512S_H_
#define SPN_FL512S_H_

#include <FreeRTOS.h>

/* FL512S op-codes */
#define SPN_FL512S_CMD_WREN			0x06  // Write enable
#define SPN_FL512S_CMD_WRDI			0x04  // Write disable
#define SPN_FL512S_CMD_WRSR			0x01  // Write register
#define SPN_FL512S_CMD_RDSR			0x05  // Read status register 1
#define SPN_FL512S_CMD_READ			0x13  // Read w/ 4 byte address
#define SPN_FL512S_CMD_FAST_READ	0x0B  // Fast read 
#define SPN_FL512S_CMD_RDID			0x9F  // Read ID
// #define SPN_FL512S_CMD_SE           0xD8  // Sector erase (256kb)
#define SPN_FL512S_CMD_SE			0xDC  // Sector erase w/ 4 byte addr (256kb)
#define SPN_FL512S_CMD_BE			0xC7  // Bulk erase
#define SPN_FL512S_CMD_PP			0x12  // Page program w/ 4 byte addr
#define SPN_FL512S_CMD_DP			0xB9  // Bank register access
#define SPN_FL512S_CMD_RES			0xAB  // Read electronic signature

/* Error codes */
#define SPN_NO_ERR          0x00
#define SPN_SPI_SETUP_FAIL  0x01
#define SPN_TIMEOUT         0x02
#define SPN_INV_CORE        0x03
#define SPN_NOMEM			0x04

/* Timeouts */
#define SPN_ERASEC_TO  180000 // Erase chip timeout in ms
#define SPN_ERASEB_TO  3000 // Erase block timeout in ms
#define SPN_WRITE_TO   3*configTICK_RATE_HZ // Write data timeout in ticks

/**
 * Turn on spansion flash and init spi interface.
 * @param core_id sets the core to init spi for
 * @return an SPN_NO_ERR, SPN_SPI_SETUP_FAIL or SPN_INV_CORE
 * */
uint8_t spn_fl512s_init(unsigned int core_id);

/**
 * Read status register in spansion.
 * @return 8 bit status register
 * */
uint8_t spn_fl512s_read_status_register(void);

/**
 * Read spansion device id.
 * @param data is the array to read device id into
 * */
void spn_fl512s_read_device_id(char data[64]);

/**
 * Read data from flash.
 * @param address sets the address to read from
 * @param data should be pointer to array to store the data
 * @param len is the length of the data to read
 * */
void spn_fl512s_read_data(uint32_t addr, uint8_t *data, uint16_t len);

/**
 * Write data to flash.
 * @param address sets the address to write to
 * @param data should be pointer to array with data to write
 * @param len is the length of the data to write
  * @return an SPN_NO_ERR or SPN_TIMEOUT
 * */
uint8_t spn_fl512s_write_data(uint32_t addr, uint8_t *data, uint16_t len);

/**
 * Erase block on flash (256kb).
 * @param address sets the block address to erase (if within a block the entire block is erased)
 * @return an SPN_NO_ERR or SPN_TIMEOUT
 * */
uint8_t spn_fl512s_erase_block(uint32_t addr);

/**
 * Erase entire chips.
 * @return an SPN_NO_ERR or SPN_TIMEOUT
 * */
uint8_t spn_fl512s_erase_chip(void);

void cmd_spn_fl512s_setup(void);

#endif /* SPN_FL512S_H_ */

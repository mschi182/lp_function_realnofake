/*
 * perifericos.h
 * Header configuracion de perifericos
 * Created: 25-03-2018 3:34:22
 *  Author: Diego
 */ 

#ifndef PERIFERICOS_H_
#define PERIFERICOS_H_

#include <asf.h>

// Obtener la linea de interrupcion de un pad (Pxn donde x es una letra y n un numero)
#define GETIRQ(PAD)			(AVR32_GPIO_IRQ_0 + ((PAD)/8))

//Interrupcion boton
#define GPIO_PUSH_BUTTON_IRQ	GETIRQ(GPIO_PUSH_BUTTON_0)
#define GPIO_PUSH_BUTTON_INTLVL AVR32_INTC_INT0

//SPI ADC
#define SPI_ADC				&AVR32_SPI0
#define SPI_ADC_MISO_PIN	AVR32_SPI0_MISO_0_0_PIN
#define SPI_ADC_MISO_FUN	AVR32_SPI0_MISO_0_0_FUNCTION
#define SPI_ADC_MOSI_PIN	AVR32_SPI0_MOSI_0_0_PIN
#define SPI_ADC_MOSI_FUN	AVR32_SPI0_MOSI_0_0_FUNCTION
#define SPI_ADC_SCK_PIN		AVR32_SPI0_SCK_0_0_PIN
#define SPI_ADC_SCK_FUN		AVR32_SPI0_SCK_0_0_FUNCTION
#define SPI_ADC_CS_PIN		AVR32_SPI0_NPCS_3_0_PIN
#define SPI_ADC_CS_FUN		AVR32_SPI0_NPCS_3_0_FUNCTION
#define SPI_ADC_CS_CH		3
#define ADC_START			AVR32_PIN_PA05
#define ADC_DREADY			AVR32_PIN_PA06
#define ADC_DREADY_IRQ		GETIRQ(ADC_DREADY)
#define ADC_DREADY_INTLVL	0

//Serial J1
#define USARTJ1				&AVR32_USART3
#define USARTJ1_RXD_PIN		AVR32_USART3_RXD_0_2_PIN
#define USARTJ1_RXD_FUN		AVR32_USART3_RXD_0_2_FUNCTION
#define USARTJ1_TXD_PIN		AVR32_USART3_TXD_0_3_PIN
#define USARTJ1_TXD_FUN		AVR32_USART3_TXD_0_3_FUNCTION

extern struct spi_device spi_device_conf;
//extern const usart_options_t usartDebug_opt;

#endif /* PERIFERICOS_H_ */
//
// Created by kaminari on 10-07-18.
//
#include "langmuirProbe.h"
static const char* tag = "langmuirProbe";
// Interval between data records in milliseconds.
//const uint32_t SAMPLE_INTERVAL_MS = 100;  //10 samples per second
//uint32_t logTime;
uint16_t j;
uint16_t arreglo [1][600]; //2000 es el ideal
//int32_t diff;
const uint8_t ANALOG_COUNT = 6;
// unsigned long mil = 1000;
void sysclk_init (void); // antes: void sysclk_init (void);
// Buffer to send data to SPI slave
uint16_t txdata;
//Buffer to receive data from SPI slave
uint16_t rxdata;
spi_options_t my_spi_options={
        // The SPI channel to set up : Memory is connected to CS1
        SPI_SLAVECHIP_NUMBER,
        // Preferred baudrate for the SPI.
        1400000,//1.000.000,: valor original
        // Number of bits in each character (8 to 16).
        8,
        // Delay before first clock pulse after selecting slave (in PBA clock periods).
        0,
        // Delay between each transfer/character (in PBA clock periods).
        0,
        // Sets this chip to stay active after last transfer to it.
        1,
        // Which SPI mode to use when transmitting.
        SPI_MODE_2,
        // Disables the mode fault detection.
        // With this bit cleared, the SPI master mode will disable itself if another
        // master tries to address it.
        1
};

void una_lectura_del_adc_del_LP(void) {
    conf_pin_ADC_CS;         // Output
    conf_pin_ADC_reset;
    conf_pin_ADC_convst;
    conf_pin_ADC_BUSY;       // Input

    set_pin_ADC_CS_HIGH; //<-----prueba
    set_pin_ADC_reset_LOW; //<-----prueba
    set_pin_ADC_convst_HIGH; //<-----prueba

    spi_init (); // funcion que inicializa la comunicacion SPI

    writeHeader();
    resetADC();
    //logTime = 0;
}
void spi_init_module(void){
    //Init SPI module as master
    spi_initMaster(SPI_EXAMPLE,&my_spi_options);
    //Setup configuration for chip connected to CS1
    spi_setupChipReg(SPI_EXAMPLE,&my_spi_options,sysclk_get_pba_hz());
    //Allow the module to transfer data
    spi_enable(SPI_EXAMPLE);
}
void resetADC(void){
    //printf("\nresetADC\n");
    set_pin_ADC_convst_HIGH;
    set_pin_ADC_reset_HIGH;
    delay_ms(1);
    set_pin_ADC_reset_LOW;
    delay_ms(1);
    set_pin_ADC_convst_LOW;
}
void startConversion(void){
    //printf("\nstartConversion\n");
    set_pin_ADC_convst_LOW;
    //delay_ms(1);
    set_pin_ADC_convst_HIGH;
    while(read_pin_ADC_BUSY==1) { //#####################################
        //printf("read_pin_ADC_BUSY: %d \n", clock());
        //delay_ms(1); // <----------------------------------------prueba
    }
}
void startAcquisition(void){
    //printf("\nstartAcquisition\n");
    set_pin_ADC_convst_HIGH;
    //delay_ms(1);
    set_pin_ADC_convst_LOW;
}
void readADC(unsigned char show){
    uint16_t highbyte;
    uint16_t lowbyte;
    //uint8_t cadena_h;
    //uint8_t cadena_l;

    uint8_t txdata = 0x00;
    spi_selectChip(SPI_EXAMPLE, SPI_SLAVECHIP_NUMBER);
    while (!spi_is_tx_empty(SPI_EXAMPLE)) {}
    spi_put(SPI_EXAMPLE, txdata);
    while (!spi_is_tx_empty(SPI_EXAMPLE)) {}
    highbyte = spi_get(SPI_EXAMPLE);
    spi_put(SPI_EXAMPLE, txdata);
    while (!spi_is_tx_empty(SPI_EXAMPLE)) {}
    lowbyte = spi_get(SPI_EXAMPLE);
    spi_unselectChip(SPI_EXAMPLE, SPI_SLAVECHIP_NUMBER);
    if (show == 4) { //if (show == 1) {
        //sprintf(cadena_h, " %u", 0x00FF & highbyte);
        //printf(cadena_h);
        //printf("|");
        //printf("|");
        //sprintf(cadena_l, "%u ", 0x00FF & lowbyte);
        //printf(cadena_l);
        //cadena_h cadena_l
        //printf("%u", show);
        //arreglo[1][j] = logTime;
        //printf("%u", show);
        //sprintf(cadena_h, " %u", 0x00FF & highbyte);
        //cadena_h = (uint8_t)0x00FF & highbyte;
        //arreglo[1][j] = cadena_h;
        //printf("%u", 0x00FF & highbyte);//~~~~~~~~~~~~~~~~~~~~~
        //sprintf(cadena_l, " %u", 0x00FF & lowbyte); //]]]]]]]]]]]]]]]]]]]]]]]]]] AQUI ESTA!!!
        //cadena_l = (uint8_t)0x00FF & lowbyte;
        //printf(" | ");//~~~~~~~~~~~~~~~~~~~~~
        //printf("%u", 0x00FF & lowbyte);//~~~~~~~~~~~~~~~~~~~~~
        //arreglo[2][j] = cadena_l;
        arreglo[1][j] = ((uint16_t)(0x00FF & highbyte) << 8) | (0x00FF & lowbyte);
        //printf(" | ");//~~~~~~~~~~~~~~~~~~~~~
        //printf("%u\n", arreglo[1][j]);//~~~~~~~~~~~~~~~~~~~~~

        //printf("%u", show);
    }
    //delay_ms(1);##SIN ESTE FUNCIONA!#######
}
void writeHeader(void){
    printf("\nCada lectura (de 16 bits) se entrega como 2 bytes separados por '||'.\n");
    printf("Sampling frequency = 83,3 [Hz].\n");
    printf("Micros, HIGH_GAIN, LOW_GAIN, TEMP1, TEMP2, V5, V6 (no se utiliza).\n");
}
void logData(void){
    //printf("spiner1");
    //printf("\nlogData\n");
    startConversion();
    //printf("spiner2");
    //printf("\n %d ",logTime);  // aqui se imprime el tiempo de cada medicion.
    for (uint8_t i = 0; i < ANALOG_COUNT; i++) {
        //data[i] = readADC(1);
        //printf("i: %d",i);
        //printf("spiner3");
        readADC(i);//readADC(1);
    }
    //printf("spiner4");
    //startAcquisition(); // <--------------------prueba
}
void una_lectura_del_adc_del_LP_2(void) {
    //printf("j1 = %u \n", j);
    j = 0;
    while(j < 500){ //originalmente quiero 2000 mediciones
        //printf("j2 = %u ||", j);
        logData();
        //printf(" dato_1 = %u", arreglo [1][j]);
        //printf(" dato_2 = %u", arreglo [2][j]);
        //printf(" dato_3 = %u\n", arreglo [3][j]);
        //logTime += 1;
        j++;
    }
    /*
    j = 0;
    //printf("j3 = %u \n", j);
    for (j = 0; j < 500; j++) {

        //sd_mmc_spi_write_word(arreglo[1][j]);
        printf("final = %u\n", arreglo[1][j]);
        //printf("%u ",j);
        //printf(" = %u",arreglo[1][j]);
        //printf("||%u \n",arreglo[2][j]);
    }*/
}